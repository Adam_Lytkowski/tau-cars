package pl.edu.pjatk.taucars.domain;

import java.util.Calendar;
import java.util.Date;

public interface TimeSource {
	public Date getCurrentDate();
}
