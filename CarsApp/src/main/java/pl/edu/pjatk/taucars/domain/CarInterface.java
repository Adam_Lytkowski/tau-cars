package pl.edu.pjatk.taucars.domain;

import java.util.Date;
import java.util.List;

public interface CarInterface {
	
//public long getCurrentDate();
public void deleteCar(int id);
public void updateCar(int id, Car car);
public Car getCar(int id);
public List<Car> getAllCars();
public void addCar(Car car);

}
