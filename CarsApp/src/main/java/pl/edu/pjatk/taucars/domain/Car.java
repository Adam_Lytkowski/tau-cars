package pl.edu.pjatk.taucars.domain;

import java.util.Date;

public class Car {
	private int id;
	private String brand;
	private String model;
	private int yearOfProduction;
	private double capacity;
	private Date createdOn;
	private Date modifiedOn;
	private Date readOn;
	
	
	
	
	
	public Car(int newId, String newBrand, String newModel, int newYearOfProduction, double newCapacity){
		id = newId;
		brand = newBrand;
		model = newModel;
		yearOfProduction =  newYearOfProduction;
		capacity = newCapacity;
	}
	
	public Car(){
		id = 1;
		brand = "Opel";
		model = "Corsa";
		yearOfProduction = 2005;
		capacity = 1.4;
//		createdOn = new Date().getTime();
//		modifiedOn = new Date().getTime();
//		readOn = new Date().getTime();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getYearOfProduction() {
		return yearOfProduction;
	}
	public void setYearOfProduction(int yearOfProduction) {
		this.yearOfProduction = yearOfProduction;
	}
	public double getCapacity() {
		return capacity;
	}
	public void setCapacity(double capacity) {
		this.capacity = capacity;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Date getReadOn() {
		return readOn;
	}

	public void setReadOn(Date readOn) {
		this.readOn = readOn;
	}
}
