package pl.edu.pjatk.taucars.resources.stepdefs;

import pl.edu.pjatk.taucars.domain.*;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import pl.edu.pjatk.taucars.service.CarsService;
import cucumber.api.java.en.Then;
import static org.junit.Assert.*;

import java.util.List;

public class Stepdefs {
	
	private CarsService db;
	private List<Car> carsFoundByRegex;
	
	@Given("^database is created$")
	public void databaseIsCreated() throws Exception {
	    db = new CarsService();
	    TimeSource timeSource = new Time();
	    db.setTimeSource(timeSource);
	    assertNotNull(db);
	}

	@Given("^I add cars to database$")
	public void iAddCarsToDatabase(List<Car> cars) throws Exception {
	   for(Car car: cars){
		   db.addCar(car);
	   }
	   assertEquals(cars.size(), db.getAllCars().size());
	}

	@When("^I search models with regex \"([^\"]*)\"$")
	public void iSearchModelsWithRegex(String regex) throws Exception {
	   carsFoundByRegex = db.searchCarModelsByRegex(regex);
	}

	@Then("^I should see (\\d+) cars$")
	public void iShouldSeeCars(int carsNumberExpected) throws Exception {
	   assertEquals(carsNumberExpected, carsFoundByRegex.size());
	}

	
	@When("^I delete cars from the list$")
	public void iDeleteCarsFromTheList(List<Car> cars) throws Exception {
	   int expectedCarListSize = db.getAllCars().size() - cars.size();
		db.deleteCarsWithList(cars);
	   assertEquals(expectedCarListSize, db.getAllCars().size());
	}

	@Then("^database should contain below cars$")
	public void databaseShouldContainBelowCars(List<Car> cars) throws Exception {
	    assertEquals(cars.size(), db.getAllCars().size());
	}

}