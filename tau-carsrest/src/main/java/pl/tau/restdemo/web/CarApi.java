package pl.tau.restdemo.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.tau.restdemo.domain.Car;
import pl.tau.restdemo.service.CarManager;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Simple web api demo -- try implementning post method
 * 
 * Created by tp on 24.04.17.
 */
@RestController
public class CarApi {

    @Autowired
    CarManager carManager;

    @RequestMapping("/")
    public String index() {
        return "This is non rest, just checking if everything works.";
    }

    @RequestMapping(value = "/car/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Car getCar(@PathVariable("id") Long id) throws SQLException {
        return carManager.getCar(id);
    }

    @RequestMapping(value = "/cars", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Car> getCars(@RequestParam(value = "filter", required = false) String f) throws SQLException {
        List<Car> cars = new LinkedList<Car>();
        for (Car c : carManager.getAllCars()) {
            if (f == null) {
                cars.add(c);
            } else if (c.getBrand().contains(f)) {
                cars.add(c);
            }
        }
        return cars;
    }

    @RequestMapping(value = "/car",
        method = RequestMethod.POST,
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public Car addCar(@RequestBody Car c) {
        if (carManager.addCar(c) < 1) return null;
        return c;
    }

    @RequestMapping(value = "/car/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Long deleteCar(@PathVariable("id") Long id) throws SQLException {
        return new Long(carManager.deleteCar(carManager.getCar(id)));
    }

    @RequestMapping(value = "/cars", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteCars() throws SQLException {
        carManager.deleteAll();
    }
    
    
    @RequestMapping(value = "/car",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
        public Car updateCar(@RequestBody Car c) throws SQLException {
            if (carManager.updateCar(c) <= 0) return null;
            return c;
        }
}
