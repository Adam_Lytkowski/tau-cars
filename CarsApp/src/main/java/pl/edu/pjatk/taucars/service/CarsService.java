package pl.edu.pjatk.taucars.service;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;

import pl.edu.pjatk.taucars.domain.Car;
import pl.edu.pjatk.taucars.domain.CarInterface;
import pl.edu.pjatk.taucars.domain.TimeSource;

public class CarsService implements CarInterface{
	
	private List<Car> db = new ArrayList<Car>();
	private TimeSource timeSource; 
	private boolean createdOnTimestamp=true;
	private boolean modifiedOnTimestamp=true;
	private boolean readOnTimestamp=true;
	
	
	
	public void addCar(Car car) {
		for(Car carFromDb: db){
			if(carFromDb.getId() == car.getId()){
				throw new IllegalArgumentException();
			}
		}
		if(createdOnTimestamp==true){
		car.setCreatedOn(this.getCurrentDate());
		}
		db.add(car);
	}
	
	public List<Car> getAllCars(){
		return db;
	}
	
	public Car getCar(int id){
		for(Car c : db){
			if(c.getId() == id){
				if(readOnTimestamp==true){
					c.setReadOn(getCurrentDate());	
				}
			return c;
			}
		}
		throw new ArrayIndexOutOfBoundsException();
		
	}
	
	public void updateCar(int id, Car car){
		int i=0;
		for(Car c : db){
			if(c.getId() == id){
				c.setBrand(car.getBrand());
				c.setModel(car.getModel());
				c.setYearOfProduction(car.getYearOfProduction());
				c.setCapacity(car.getCapacity());
				if(modifiedOnTimestamp==true){
				c.setModifiedOn(getCurrentDate());
				}
				i=1;
			}
			}
		if(i==0){
				throw new ArrayIndexOutOfBoundsException();
			
		}
						
	}
	
	public void deleteCar(int id){
		int i=0;
		for(Car c : db){
			if(c.getId() == id){
				i=1;
			}
		}
		db.remove(id -1);
		if(i==0)
			throw new ArrayIndexOutOfBoundsException();
	}

	public Date getCurrentDate() {
		return this.timeSource.getCurrentDate();
	}
	
	public void setTimeSource(TimeSource time) {
		this.timeSource = time;
	}
	
	public Date getCreatedOnTimestamp(int id){
		for(Car c : db){
			if(c.getId() == id){
				if(c.getCreatedOn()==null){
					throw new IndexOutOfBoundsException();
				}else
			return c.getCreatedOn();
				}
			}
		throw new ArrayIndexOutOfBoundsException();
		}
	

public Date getReadOnTimestamp(int id){
	for(Car c : db){
		if(c.getId() == id){	
			if(c.getReadOn()==null){
				throw new IndexOutOfBoundsException();
			}else
		return c.getReadOn();
			}
		}
	throw new ArrayIndexOutOfBoundsException();
	}

public Date getModifiedOnTimestamp(int id){
	for(Car c : db){
		if(c.getId() == id){
			if(c.getModifiedOn()==null){
				throw new IndexOutOfBoundsException();
			}else
		return c.getModifiedOn();
			}
		}
	throw new ArrayIndexOutOfBoundsException();
	}

public void turnOffCreatedOnTimestamp(){
	createdOnTimestamp= false;
}

public void turnOnCreatedOnTimestamp(){
	createdOnTimestamp= true;
}

public void turnOffReadOnTimestamp(){
	readOnTimestamp= false;
}

public void turnOnReadOnTimestamp(){
	readOnTimestamp= true;
}

public void turnOffModifiedOnTimestamp(){
	modifiedOnTimestamp= false;
}

public void turnOnModifiedOnTimestamp(){
	modifiedOnTimestamp= true;
}

public List<Car> searchCarModelsByRegex(String regex){
	List<Car> carsFoundByRegex = new ArrayList<Car>();
	
	for(Car car : db) {
		if(car.getModel().matches(regex)){
			carsFoundByRegex.add(car);
		}
	}
	return carsFoundByRegex;
}

public void deleteCarsWithList(List<Car> carsToDelete){
	List<Car> toRemove = new ArrayList<Car>();
	
	for(Car car : db) {
		for(Car carToDelete : carsToDelete){
			if(car.getId()==carToDelete.getId() && car.getBrand().equals(carToDelete.getBrand())
			&& car.getModel().equals(carToDelete.getModel()) && car.getCapacity() == carToDelete.getCapacity()
			&& car.getYearOfProduction() == carToDelete.getYearOfProduction()){
				toRemove.add(car);
			}
		}
	}
	
db.removeAll(toRemove);
}

}
