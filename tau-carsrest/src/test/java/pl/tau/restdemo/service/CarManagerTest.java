package pl.tau.restdemo.service;

// przyklad na podstawie przykladow J. Neumanna
import static org.junit.Assert.*;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.context.web.WebAppConfiguration;

import pl.tau.restdemo.domain.Car;

public class CarManagerTest {

	CarManager carManager;

	private final static String BRAND_1 = "Audi";
	private final static String MODEL_1 = "A3";
	private final static int YEAROFPRODUCTION_1 = 2002;
	private final static int CAPACITY_1 = 19;

	@Before
	public void setup() throws SQLException {
		carManager  = new CarManagerImpl();
	}

	@After
	public void cleanup() throws SQLException {
	}

	@Test
	public void checkConnection() {
		assertNotNull(carManager.getConnection());
	}

	@SuppressWarnings("deprecation")
	@Test
	public void checkAdding() throws SQLException {
		Car car = new Car();
		car.setBrand(BRAND_1);
		car.setModel(MODEL_1);
		car.setYearOfProduction(YEAROFPRODUCTION_1);
		car.setCapacity(CAPACITY_1);

		carManager.deleteAll();
		assertEquals(1, carManager.addCar(car));

		List<Car> cars = carManager.getAllCars();
		Car carRetrieved = cars.get(0);

		assertEquals(BRAND_1, carRetrieved.getBrand());
		assertEquals(MODEL_1, carRetrieved.getModel());
		assertEquals(YEAROFPRODUCTION_1, carRetrieved.getYearOfProduction());
		assertEquals(CAPACITY_1, carRetrieved.getCapacity());
	}

}
