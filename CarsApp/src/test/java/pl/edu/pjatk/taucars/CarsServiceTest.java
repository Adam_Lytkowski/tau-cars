package pl.edu.pjatk.taucars;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.edu.pjatk.taucars.domain.Car;
import pl.edu.pjatk.taucars.domain.CarInterface;
import pl.edu.pjatk.taucars.domain.TimeSource;
import pl.edu.pjatk.taucars.service.CarsService;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
//@RunWith(JUnit4.class)
public class CarsServiceTest {
	@Mock TimeSource timeSource;
	
	Date mockTime = new Date();
	CarsService db;
	
	@Before
	public void setMockTime(){
		when(timeSource.getCurrentDate()).thenReturn(mockTime);
		
		db = new CarsService();
		db.setTimeSource(timeSource);
	}
	
	@Test
	public void createCarObject() {
		Car car = new Car();
		assertNotNull(car);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void createCarsTest(){
		Car car1 = new Car(1, "Fiat", "Punto", 2001, 1.2);
		Car car2 = new Car(1, "Fiat", "Punto", 2001, 1.2);
		
		db.addCar(car1);
		db.addCar(car2);
	}
	
	@Test
	public void readAllCarsTest(){
		Car car1 = new Car(1, "Fiat", "Punto", 2001, 1.2);
		Car car2 = new Car(2, "Opel", "Astra", 2005, 1.7);
		
		db.addCar(car1);
		db.addCar(car2);
		
		//stworz liste z samochodami
		List<Car> newDb = new ArrayList<Car>();
		newDb.add(car1);
		newDb.add(car2);
		//dodaj samochod do listy
		
		assertEquals(newDb, db.getAllCars());
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void readCarbyIdTest(){
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);	
		
		db.addCar(car1);
		
		db.getCar(2);
	}
	
	
	@Test
	public void updateCarBrandTest(){
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		Car car2 = new Car(1, "Fiat", "Punto", 2002, 1.2);
		
		db.addCar(car1);
	
		db.updateCar(1, car2);
		assertEquals(car2.getBrand(), db.getCar(1).getBrand());
	}
	
	@Test
	public void updateCarModelTest(){
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		Car car2 = new Car(1, "Fiat", "Punto", 2002, 1.2);
		
		db.addCar(car1);
	
		db.updateCar(1, car2);
		assertEquals(car2.getModel(), db.getCar(1).getModel());
	}
	
	@Test
	public void updateCarYearOfProductionTest(){
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		Car car2 = new Car(1, "Fiat", "Punto", 2002, 1.2);
		
		db.addCar(car1);
	
		db.updateCar(1, car2);
		assertEquals(car2.getYearOfProduction(), db.getCar(1).getYearOfProduction());
	}
	
	@Test
	public void updateCarCapacityTest(){
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		Car car2 = new Car(1, "Fiat", "Punto", 2002, 1.2);
		
		db.addCar(car1);
	
		db.updateCar(1, car2);
		assertEquals(car2.getCapacity(), db.getCar(1).getCapacity(), 0.001);
	}
	
	@Test
	public void updateCarTest(){
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		Car car2 = new Car(2, "Fiat", "Punto", 2002, 1.2);
		Car car3 = new Car(3, "Volkswagen", "Golf 4", 2002, 1.9);
		Car car4 = new Car(4, "Volvo", "s60", 2002, 2.0);
		
		Car car5 = new Car(2, "Fiat", "Grande Punto", 2008, 1.4);
		
		db.addCar(car1);
		db.addCar(car2);
		db.addCar(car3);
		db.addCar(car4);
		
		db.updateCar(2, car5);
		assertNotEquals(car3.getModel(), db.getCar(1).getModel());
		assertEquals(car5.getModel(), db.getCar(2).getModel());
	}
	
	
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void checkUpdateCarIdMatchTest(){
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		Car car2 = new Car(2, "Fiat", "Punto", 2002, 1.2);
		
		db.addCar(car1);
	
		db.updateCar(2, car2);
	}
	
	@Test(expected=ArrayIndexOutOfBoundsException.class)
	public void deleteCarTest(){
		
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		Car car2 = new Car(2, "Fiat", "Punto", 2002, 1.2);
		
		db.addCar(car1);
		db.addCar(car2);
		
		db.deleteCar(2);
		
		db.getCar(2);
	}
	
	
	@Test
	public void checkTimestampOnReadCarTest(){
		
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		
		
		db.addCar(car1);
		
		assertEquals(mockTime, db.getCar(1).getReadOn());
	}
	
	@Test
	public void checkTimestampOnCreateCarTest(){
		
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		
		db.addCar(car1);
		
		assertEquals(mockTime, db.getCar(1).getCreatedOn());
	}
	
	@Test
	public void checkTimestampOnModifyCarTest(){

		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		Car car2 = new Car(1, "Fiat", "Punto", 2002, 1.2);
		
		db.addCar(car1);
		db.updateCar(1, car2);
		
		assertEquals(mockTime, db.getCar(1).getModifiedOn());
			}
	
	@Test
	public void getCreatedOnTimestampTest(){
		
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		
		db.addCar(car1);
		
		assertEquals(mockTime, db.getCreatedOnTimestamp(1));
	}
	
	@Test
	public void getReadOnTimestampTest(){
	
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		
		db.addCar(car1);
		db.getCar(1);
		
		assertEquals(mockTime, db.getReadOnTimestamp(1));
	}
	
	
	@Test
	public void getModifiedOnTimestampTest(){
		
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		Car car2 = new Car(1, "Fiat", "Punto", 2002, 1.2);
	
		db.addCar(car1);
		db.updateCar(1, car2);
		
		assertEquals(mockTime, db.getModifiedOnTimestamp(1));
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void turnOffCreatedOnTimestampTest(){
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		
		db.turnOffCreatedOnTimestamp();
		
		db.addCar(car1);
		
		db.getCreatedOnTimestamp(1);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void turnOffModifiedOnTimestampTest(){
		
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		Car car2 = new Car(1, "Opel", "Corsa", 2007, 1.3);
		
		db.addCar(car1);
		db.turnOffModifiedOnTimestamp();
		db.updateCar(1, car2);
		
		db.getModifiedOnTimestamp(1);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void turnOffReadOnTimestampTest(){
		
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		
		db.turnOffReadOnTimestamp();
		
		db.addCar(car1);
		db.getCar(1);
		
		db.getReadOnTimestamp(1);
	}
	
	@Test
	public void turnOnCreatedOnTimestampTest(){
		
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		
		db.turnOffCreatedOnTimestamp();
	
		db.turnOnCreatedOnTimestamp();
		
		db.addCar(car1);
		
		assertEquals(mockTime, db.getCreatedOnTimestamp(1));
	}
	
	@Test
	public void turnOnReadOnTimestampTest(){
		
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
	
		db.turnOffReadOnTimestamp();
		db.turnOnReadOnTimestamp();
		
		
		db.addCar(car1);
		db.getCar(1);
		
		assertEquals(mockTime, db.getReadOnTimestamp(1));
	}
	
	@Test
	public void turnOnModifiedOnTimestampTest(){
		
		Car car1 = new Car(1, "Opel", "Astra", 2005, 1.7);
		Car car2 = new Car(1, "Fiat", "Punto", 2002, 1.2);
		
		db.turnOffModifiedOnTimestamp();
		db.turnOnModifiedOnTimestamp();
		
		
		db.addCar(car1);
		db.updateCar(1, car2);
		
		assertEquals(mockTime, db.getModifiedOnTimestamp(1));
	}

}
