Feature: Regex

Scenario: Searching car models by regex
    Given database is created
    And I add cars to database
        | id | brand  	| model		   | yearOfProduction | capacity |
        | 1  | Opel 	| Astra H      |	2005		  | 1.7 	 |
        | 2  | Opel	    | Astra G	   |	2006		  |	1.2		 |
        | 3  | Opel     | Corsa  	   |	2002		  |	1.2		 |
        | 4  | Volkswagen | Golf   |	2004 		  |	1.9		 |
        | 5  | Skoda  | Octavia    |	2010		  |	1.6		 |
        | 6  | Toyota | Corolla	   |	2009		  |	1.4		 |
    When I search models with regex "(.*)Astra(.*)"
    Then I should see 2 cars
    
    
 Scenario: Deleting cars using list of cars
 	Given database is created
 	And I add cars to database
 		| id | brand  	| model		   | yearOfProduction | capacity |
        | 1  | Opel 	| Astra H      |	2005		  | 1.7 	 |
        | 2  | Opel	    | Astra G	   |	2006		  |	1.2		 |
        | 3  | Opel     | Corsa  	   |	2002		  |	1.2		 |
        | 4  | Volkswagen | Golf   	   |	2004 		  |	1.9		 |
        | 5  | Skoda  | Octavia    	   |	2010		  |	1.6		 |
        | 6  | Toyota | Corolla	   	   |	2009		  |	1.4		 |
    When I delete cars from the list
    	| id | brand    | model        | yearOfProduction | capacity |
    	| 1  | Opel 	| Astra H      |	2005		  | 1.7 	 |
    	| 3  | Opel     | Corsa   	   |	2002		  |	1.2		 |
        | 4  | Volkswagen | Golf   	   |	2004 		  |	1.9		 |
    Then database should contain below cars
    	|id  | brand    | model        | yearOfProduction | capacity |
        | 2  | Opel	    | Astra G	   |	2006		  |	1.2		 |
        | 5  | Skoda  | Octavia    	   |	2010		  |	1.6		 |
        | 6  | Toyota | Corolla	   	   |	2009		  |	1.4		 |